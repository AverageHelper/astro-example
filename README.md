# $REPO_NAME

$REPO_DESCRIPTION

# Template Features

Includes several functions out of the box:

- Source indents using tabs, without prescribing a tab width, [for accessibility](https://www.reddit.com/r/javascript/comments/c8drjo/).
- Default OpenGraph config, using the name and description of the repository. (Change this at `src/layouts/Layout.astro`.)
- Static site generation, with no client-side JS, by default.
- Write pages in Markdown or Astro format.
- Machine-readable sitemap generates automatically from Markdown content.
- Simple styles that work on all reasonable screen sizes.
- Automatic light and dark color modes, depending on the device preference. (Change the color scheme at `src/styles/colors.css`.)
- Stylesheets generate inline by default, so webpage styles load quickly.
- NPM configured to install exact package versions by default, to mitigate dependency injection attacks.

A few functions you'll have to configure yourself:

- `public/humans.txt` is a basic template. Please update that to reflect your authorship.
- Update `astro.config.ts` and `public/robots.txt` with your site URL.
- Human-readable sitemap at `src/pages/sitemap.astro` loads `.astro` and `.md` files from the root of the `pages/` directory by default, and the `humans.txt` file. Update this file as your site grows.
- Predefined response headers in `src/middleware.ts` will only be sent when Astro builds to a dynamic webserver endpoint, not with the default static site generation. To send custom headers, consult your webhost's documentation.
- Update the `FUNDING.yml` file with your own funding source.
- Update the `LICENSE` file and the `"license"` entry in `package.json` with a different code license, if you'd rather not use the default GPLv3.

# Usage

We use [Astro](https://astro.build) to generate HTML on demand.

## Install Dependencies

We'll need these for our build tools.

```sh
npm ci
```

## Make changes

The contents of the `/src` directory gets compiled into static HTML assets.

The contents of the `/public` directory get copied into the output folder as-is.

The build result lives in `/dist`, and gets sent verbatim to the webhost. Please do not modify these files directly.

Use the following command to run a live webserver:

```sh
npm start
```

## Build the site

This command will build the site and update `/dist`:

```sh
npm run build
```

## Contributing

This project's initial commit was generated from [a template by AverageHelper](https://git.average.name$TEMPLATE_LINK). If you would like to contribute to that template, please file an issue or pull request there. You may sign in or create an account directly, or use one of several OAuth 2.0 providers.

This project currently lives at [git.average.name](https://git.average.name$REPO_LINK). Issues or pull requests about this project should be filed here.
