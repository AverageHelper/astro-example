/* humans.txt file -- please visit https://humanstxt.org/ for details */

/* TEAM */

operator:   AverageHelper
pronouns:   she/her
site:       https://average.name
fediverse:  https://average.name/@average
location:   USA

/* SITE */

standards:  HTML5, CSS3
software:   Node, NPM, ESLint, Prettier, TypeScript, Astro
