---
layout: "@/layouts/Layout.astro"
---

# Hello, world!

Add some static content to this page at `src/pages/index.md`. For more complex pages, rename this file to `index.astro`, or add another `.astro` file to the `src/pages` directory.
