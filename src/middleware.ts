import { defineMiddleware } from "astro:middleware";

// This is never called when generating statically:
export const onRequest = defineMiddleware(async (context, next) => {
	const res = await next();

	// CORS
	if (context.site) {
		res.headers.set("Access-Control-Allow-Origin", context.site.href);
	}

	// Disable caching
	res.headers.set("Vary", "*");

	// Browsers should avoid loading from unknown domains
	// TODO: Ditch unsafe-inline in static pages
	res.headers.set(
		"Content-Security-Policy",
		"default-src 'none'; img-src 'self' data:; media-src 'self' data:; style-src 'self' 'unsafe-inline'; frame-ancestors 'self'; upgrade-insecure-requests",
	);

	// Browsers should disable features we do not use
	res.headers.set(
		"Permissions-Policy",
		"accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), clipboard-read=(), clipboard-write=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), gamepad=(), geolocation=(), gyroscope=(), identity-credentials-get=(), idle-detection=(), interest-cohort=(), keyboard-map=(), local-fonts=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-create=(), publickey-credentials-get=(), screen-wake-lock=(), serial=(), speaker-selection=(), storage-access=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=()",
	);

	// Browsers should prefer HTTPS
	res.headers.set("Strict-Transport-Security", "max-age=2592000; includeSubDomains; preload");

	// Browsers should be cautious about loading resources from other origins
	res.headers.set("Cross-Origin-Embedder-Policy", "require-corp");

	// Browsers should not load cross-origin documents
	res.headers.set("Cross-Origin-Opener-Policy", "same-origin");

	// Browsers should not load this resource from another site
	res.headers.set("Cross-Origin-Resource-Policy", "same-site");

	// Browsers shouldn't expect executable MIME types from requests for non-executable files
	res.headers.set("X-Content-Type-Options", "nosniff");

	// Browsers shouldn't load us in an iframe unless the page is ours
	res.headers.set("X-Frame-Options", "SAMEORIGIN");

	// Browsers shouldn't send the `Referer` header with sent requests
	res.headers.set("Referrer-Policy", "no-referrer");

	// Miscellaneous
	res.headers.set("X-Clacks-Overhead", "GNU Nex Benedict");

	// Don't show pages.dev deployment in search results
	if (context.url.hostname.endsWith("pages.dev")) {
		res.headers.set("X-Robots-Tag", "noindex");
	}

	return res;
});
