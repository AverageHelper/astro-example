/* eslint-disable import/no-default-export */

import { defineConfig } from "astro/config";
import { Temporal } from "temporal-polyfill";
import sitemap from "@astrojs/sitemap";

// Prefer Temporal over legacy Date, and convert where APIs require legacy Date
const today = new Date(Temporal.Now.instant().epochMilliseconds);

// https://astro.build/config
export default defineConfig({
	site: "https://astro.example.com",
	root: ".", // Project root is the working directory
	outDir: "./dist", // Build to `/dist`
	publicDir: "./public", // Static assets live in `/public`
	srcDir: "./src", // Component sources live in `/src`
	output: "static", // Disable SSR
	build: {
		format: "file", // Build HTML pages at root, not in subdirectories
		assets: "assets", // Call the build assets folder "assets" instead of "_astro"
		inlineStylesheets: "always", // Inline stylesheets so they load quickly
	},
	devToolbar: {
		enabled: false, // Don't show dev controls in the webpage
	},
	compressHTML: false, // Pretty output for nerds
	integrations: [
		sitemap({
			changefreq: "weekly",
			lastmod: today, // Last modified on [today]
		}),
	],
});
